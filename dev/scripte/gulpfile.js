var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    autoprefix  = require('gulp-autoprefixer'),
    concat      = require('gulp-concat'),
    rename      = require('gulp-rename'),
    uglify      = require('gulp-uglify'),
    sourcemaps  = require('gulp-sourcemaps'),
    filesExist  = require('files-exist'),
    plumber     = require('gulp-plumber'),
    notify      = require('gulp-notify');

/**
 * Hier müssen die Pfade relativ vom Gulpfile zum Less, CSS und Javascript angegeben werden.
 */

var paths = {
    scss: {
        baseFile: '../src/scss/base.scss',
        src: '../src/scss/**/*.scss'
    },
    css: {
        dist: '../../build/css'
    },
    js: {
        baseFile: '../src/js/main.js',
        dist: '../../build/js'
    },
    map: {
        dist: '../../build/maps'
    }
}

/**
 * Compile sass, js und jshead
 */
gulp.task('default', ['sass', 'js', 'jshead'], function () {});

/**
 * Compile sass
 */
gulp.task('sass', function () {
    return gulp.src(filesExist(paths.scss.baseFile))
    .pipe(plumber({ errorHandler: function(err) {
        notify.onError({
            title: "Gulp error in " + err.plugin,
            message:  err.toString()
        })(err);
    }}))
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename('main.min.css'))
    .pipe(autoprefix())
    .pipe(sourcemaps.write(paths.map.dist))
    .pipe(gulp.dest(paths.css.dist));
});

/**
 * Compile js zur Einbindung vor schließendem </body>
 */
gulp.task('js', function(){
    return gulp.src(filesExist([
        // Beispieleinbindung von einem Script aus den node_modules
        // 'node_modules/slick-carousel/slick/slick.js',
        paths.js.baseFile
    ]))
    .pipe(plumber({ errorHandler: function(err) {
        notify.onError({
            title: "Gulp error in " + err.plugin,
            message:  err.toString()
        })(err);
    }}))
    .pipe(sourcemaps.init())
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest(paths.js.dist))
    .pipe(uglify())
    .pipe(sourcemaps.write(paths.map.dist))
    .pipe(gulp.dest(paths.js.dist));
});

/**
 * HINWEIS: "jshead" WIRD IN KEINEM WATCH-PROZESS AUSGEFÜHRT
 * Compile js zur Einbindung in den <head>
 * head.min.js wird erst von Gulp generiert, wenn mindestens 1 File included wird
 */
gulp.task('jshead', function(){
    return gulp.src(filesExist([
        // Beispieleinbindung von einem modernizr Script
        // '../src/js/modernizr-custom.js',
    ]))
    .pipe(sourcemaps.init())
    .pipe(concat('head.min.js'))
    .pipe(gulp.dest(paths.js.dist))
    .pipe(uglify())
    .pipe(sourcemaps.write(paths.map.dist))
    .pipe(gulp.dest(paths.js.dist));
});

/**
 * Watch all
 */
gulp.task('watch', ['sass', 'js'], function () {
    gulp.watch(paths.scss.src, ['sass']);
    gulp.watch(paths.js.baseFile, ['js']);
});

/**
 * Watch sass
 */
gulp.task('sasswatch', ['sass'], function () {
    gulp.watch(paths.scss.src, ['sass']);
});

/**
 * Watch js
 */
gulp.task('jswatch', ['js'], function () {
    gulp.watch(paths.js.baseFile, ['js']);
});