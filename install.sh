cd dev/scripte/

echo ''
echo '#######################################################';
echo ''
echo 'Projekt wird installiert ...';
echo ''
echo '#######################################################';
echo ''

yarn install
echo ''
echo '#######################################################';
echo ''
echo 'yarn/npm install wurde ausgeführt'
echo ''
echo '#######################################################';
echo ''

composer install
echo ''
echo '#######################################################';
echo ''
echo 'composer install wurde ausgeführt';
echo ''
echo '#######################################################';
echo ''

echo ''
echo '#######################################################';
echo ''
echo 'Projekt wurde installiert';
echo ''
echo '#######################################################';
echo ''

./gulpTasks.sh

gulp watch