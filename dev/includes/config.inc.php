<?php
ini_set("display_errors", 1);
ini_set('error_reporting', E_ALL);

$params = array (

);


require_once dirname(__FILE__) . '/../scripte/vendor/autoload.php';
$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/templates');
$twig = new Twig_Environment($loader, array(
    'cache' => dirname(__FILE__) . '/../cache',
    'auto_reload' => true
));