<?php

require_once('includes/config.inc.php');

if(!isset($_GET['p']) || $_GET['p'] == '' )
{
    $p = 'DEFAULT';
}
else
{
    $p = $_GET['p'];
};

switch($p)
{
	default:
		$template = 'layout/master.html';
		break;
	case 10:
	case 'elements':
        $template = 'elements.html';
        break;
    case 100:
    case '404':
        $template = '404.html';
        break;
}

$template = $twig->loadTemplate($template);
echo $template->render($params);